<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Thêm mới tài khoản</title>
</head>
<body>
	<h1>ĐĂNG KÝ TÀI KHOẢN</h1>
	<form action="registerUser.htm" method="post">
		<div>
			<label>Tài khoản : </label><input type="text" name="username">
		</div>
		<div>
			<label>Mật khẩu : </label><input type="password" name="password">
		</div>
		<div>
			<label>Họ và tên : </label><input type="text" name="fullname">
		</div>
		<div>
			<label>Tuổi : </label><input type="text" name="age">
		</div>
		<div>
			<label>Giới tính : </label> 
			<input name="gender" type="radio" value="1" checked="checked"/>Nam 
			<input name="gender" type="radio" value="0" />Nữ
		</div>
		<div>
			<label>Điện thoại : </label><input type="text" name="phone">
		</div>
		<div>
			<label>Email : </label><input type="text" name="email">
		</div>
		<div>
			<label>Địa chỉ : </label><input type="text" name="address">
		</div>

		<br>
		<div>
			<button>Đăng ký</button>
			<button>
				<a href="login.htm">Scancel</a>
			</button>
		</div>
		${message}
	</form>
</body>
</html>