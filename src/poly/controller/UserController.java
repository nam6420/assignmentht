package poly.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import poly.entity.ThongTinKhachHang;
import poly.entity.User;

@Controller
public class UserController {

	@Autowired
	SessionFactory factory;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping()
	public String home() {
		return "home";
	}

	// Login
	@Transactional
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(ModelMap model, HttpServletRequest request) {
		String username = request.getParameter("tenDangNhap");
		String password = request.getParameter("matKhau");
		Session session = factory.getCurrentSession();
		User user = (User) session.get(User.class, username);
		if (user == null) {
			model.addAttribute("message", "Tên đăng nhập không tồn tại !");
			return "login";
		} else if (!password.equals(user.getMatKhau())) {
			model.addAttribute("message", "Sai mật khẩu !");
			return "login";
		}
		return "home";
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.GET)
	public String add(ModelMap model) {
		return "register";
	}

	@Transactional
	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	public String save(ModelMap model, @ModelAttribute("register") User user) {

		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		User user1 = new User();
		user1 = user;
		user1.setTrangThai(true);

		try {
			String hql = "COUNT(maKhachHang) Form User";
			Query query  = session.createQuery(hql);
			System.out.println("Xin chao 1 " + query);
			user1.setMaKhachHang("KH000" + hql);
			session.save(user);
			t.commit();
			model.addAttribute("message", "Đăng ký thành công");

		} catch (Exception e) {
			t.rollback();
			model.addAttribute("message", "Đăng ký thất bại");
			e.printStackTrace();// in thế sao fixx error

		} finally {
			session.close();

		}
		return "redirect:/registerUser.htm";
	}

}
