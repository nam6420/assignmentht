package poly.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ThongTinKhachKhang")
public class User {

	@Id
	@Column(name = "MaKhachHang")
	private String maKhachHang;

	@Column(name = "TenkhachHang")
	private String tenKhachHang;

	@Column(name = "TenDangNhap")
	private String tenDangNhap;

	@Column(name = "MatKhau")
	private String matKhau;

	@Column(name = "DiaChi")
	private String diaChi;

	@Column(name = "GioiTinh")
	private boolean GioiTinh;

	@Column(name = "Tuoi")
	private int tuoi;

	@Column(name = "SoDienThoai")
	private int soDienThoai;

	@Column(name = "Email")
	private String email;

	@Column(name = "Anh")
	private String anh;

	@Column(name = "LienKetFB")
	private boolean lienKetFB;

	@Column(name = "TrangThai")
	private boolean trangThai;

	public User(String maKhachHang, String tenKhachHang, String tenDangNhap, String matKhau, String diaChi,
			boolean gioiTinh, int tuoi, int soDienThoai, String email, String anh, boolean lienKetFB,
			boolean trangThai) {
		super();
		this.maKhachHang = maKhachHang;
		this.tenKhachHang = tenKhachHang;
		this.tenDangNhap = tenDangNhap;
		this.matKhau = matKhau;
		this.diaChi = diaChi;
		GioiTinh = gioiTinh;
		this.tuoi = tuoi;
		this.soDienThoai = soDienThoai;
		this.email = email;
		this.anh = anh;
		this.lienKetFB = lienKetFB;
		this.trangThai = trangThai;
	}

	public User() {
		super();
	}

	public String getMaKhachHang() {
		return maKhachHang;
	}

	public void setMaKhachHang(String maKhachHang) {
		this.maKhachHang = maKhachHang;
	}

	public String getTenKhachHang() {
		return tenKhachHang;
	}

	public void setTenKhachHang(String tenKhachHang) {
		this.tenKhachHang = tenKhachHang;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public String getMatKhau() {
		return matKhau;
	}

	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public boolean isGioiTinh() {
		return GioiTinh;
	}

	public void setGioiTinh(boolean gioiTinh) {
		GioiTinh = gioiTinh;
	}

	public int getTuoi() {
		return tuoi;
	}

	public void setTuoi(int tuoi) {
		this.tuoi = tuoi;
	}

	public int getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(int soDienThoai) {
		this.soDienThoai = soDienThoai;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAnh() {
		return anh;
	}

	public void setAnh(String anh) {
		this.anh = anh;
	}

	public boolean isLienKetFB() {
		return lienKetFB;
	}

	public void setLienKetFB(boolean lienKetFB) {
		this.lienKetFB = lienKetFB;
	}

	public boolean isTrangThai() {
		return trangThai;
	}

	public void setTrangThai(boolean trangThai) {
		this.trangThai = trangThai;
	}

}
