package poly.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ThongTinKhachKhang")
public class ThongTinKhachHang {
	@Id
	@Column(name = "MaKhachHang")
	private String maKhachHang;

	@Column(name = "TenkhachHang")
	private String tenKhachHang;

	@Column(name = "Email")
	private String mail;

	@Column(name = "Tuoi")
	private int tuoi;

	@Column(name = "SoDienThoai")
	private int dienThoai;

	@Column(name = "GioiTinh")
	private boolean gioiTinh;

	@Column(name = "Anh")
	private String anh;

	@Column(name = "DiaChi")
	private String diaChi;

	@Column(name = "TenDangNhap")
	private String tenDangNhap;

	@Column(name = "MatKhau")
	private String matKhau;

	@Column(nullable=true ,name = "LienKetFB")
	private boolean lienKetFB;

	@Column(name = "TrangThai")
	private boolean trangThai;

	public String getMaKhachHang() {
		return maKhachHang;
	}

	public void setMaKhachHang(String maKhachHang) {
		this.maKhachHang = maKhachHang;
	}

	public String getTenKhachHang() {
		return tenKhachHang;
	}

	public void setTenKhachHang(String tenKhachHang) {
		this.tenKhachHang = tenKhachHang;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getTuoi() {
		return tuoi;
	}

	public void setTuoi(int tuoi) {
		this.tuoi = tuoi;
	}

	public int getDienThoai() {
		return dienThoai;
	}

	public void setDienThoai(int dienThoai) {
		this.dienThoai = dienThoai;
	}

	public boolean isGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(boolean gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public String getAnh() {
		return anh;
	}

	public void setAnh(String anh) {
		this.anh = anh;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public String getMatKhau() {
		return matKhau;
	}

	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}

	public boolean isLienKetFB() {
		return lienKetFB;
	}

	public void setLienKetFB(boolean lienKetFB) {
		this.lienKetFB = lienKetFB;
	}

	public boolean isTrangThai() {
		return trangThai;
	}

	public void setTrangThai(boolean trangThai) {
		this.trangThai = trangThai;
	}

	public ThongTinKhachHang(String maKhachHang, String tenKhachHang, String mail, int tuoi, int dienThoai, boolean gioiTinh,
			String anh, String diaChi, String tenDangNhap, String matKhau, boolean lienKetFB, boolean trangThai) {
		super();
		this.maKhachHang = maKhachHang;
		this.tenKhachHang = tenKhachHang;
		this.mail = mail;
		this.tuoi = tuoi;
		this.dienThoai = dienThoai;
		this.gioiTinh = gioiTinh;
		this.anh = anh;
		this.diaChi = diaChi;
		this.tenDangNhap = tenDangNhap;
		this.matKhau = matKhau;
		this.lienKetFB = lienKetFB;
		this.trangThai = trangThai;
	} // 12 dong dung 12 ma

	public ThongTinKhachHang() {
		super();
// thế thì có khả năng là thiếu hay sai trường thôi chứ
	}
}
